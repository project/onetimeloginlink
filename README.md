# OneTime LoginLink

- This module allows authorised user to create one time login for the user.

## Table of contents

- Introduction
- Installation
- Configuration
- Maintainers

## Introduction

- Based on roles any user can create one time login for the user
  if he knows user name or user email address.

- We have form with text field which accepts user email address
  or username based on account one time login link is created.

- We set the permission for this module so only specified roles can
  see the form and create one time login link.

## Installation

- Install the OneTime LoginLink module as you would normally
  install a contributed Drupal module
  Visit <https://www.drupal.org/node/1897420/> for further information.

## Configuration

1. Navigate to Administration > Extend and enable the module.
1. Use the [Configure] (/admin/onetime-loginlink) link and
   Enter User Name or User Email.
1. Click on "Create" button.

## Maintainers

- Rajan Kumar - [rajan-kumar](https://www.drupal.org/u/rajan-kumar)
- Dipali Goel - [dipali.goel25](https://www.drupal.org/u/dipaligoel25)
